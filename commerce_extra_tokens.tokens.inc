<?php

/**
 * @file
 * Implements the Token API to add more commerce order tokens.
 */

/**
 * Implements hook_token_info_alter().
 */
function commerce_extra_tokens_token_info_alter(&$data) {
  $view = views_get_view('commerce_extra_tokens');

  if (!empty($view->display)) {
    foreach ($view->display as $machine_name => $display) {
      if ($machine_name != 'default') {
        // Create an order token for each view display in the view.
        $data['tokens']['commerce-order']['commerce_extra_tokens_' . $machine_name] = array(
          'name' => t('Commerce extra tokens @display', array('@display' => $display->display_title)),
          'description' => t('Renders commerce extra tokens "@display" view output', array('@display' => $display->display_title)),
        );
      }
    }
  }
}

/**
 * Implements hook_tokens().
 */
function commerce_extra_tokens_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ($type == 'commerce-order' && !empty($data['commerce-order'])) {

    foreach ($tokens as $name => $original) {
      // Determine if the token should be handled by commerce_extra_tokens
      // and which view display to use (from the machine name).
      preg_match('/commerce\_extra\_tokens\_(.*)/', $name, $matches);

      // Execute the view and replace the token with the rendered output.
      if (!empty($matches[1])) {
        $order_items = commerce_extra_tokens_token_replace($data['commerce-order'], $matches[1]);
        $replacements[$original] = $order_items;
      }
    }
  }

  return $replacements;
}
